import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PublicLayoutComponent } from './layout-module/containers/public-layout/public-layout.component';
import { LayoutWithSideMenuComponent } from './layout-module/containers/layout-with-side-menu/layout-with-side-menu.component';

const routes: Routes = [
  { path: '', loadChildren: () => import('./landing-module/landing.module').then(m => m.LandingModule)},

    
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled', preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
