import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from './layout-module/layout.module';
import { IdenaFeedService } from './services/idena-feed.service';

export const appInitializer = (IdenaFeed: IdenaFeedService) => {
  // ничего не делаем, нужно для того, чтобы инстанциировался IdenaFeedService
  return () => Promise.resolve();
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    LayoutModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center',
    }),
    HammerModule
  ],
  providers: [
    {provide: APP_INITIALIZER, useFactory: appInitializer, deps: [IdenaFeedService], multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
