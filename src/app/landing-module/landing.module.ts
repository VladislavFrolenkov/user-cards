import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { routes } from "./routes";
import { LandingComponent } from './containers/landing/landing.component';
import { LayoutModule } from 'app/layout-module/layout.module';
import { ButtonModule } from '@ui/button';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        InlineSVGModule,
        LayoutModule,
        ButtonModule
    ],
    declarations: [
        LandingComponent,
    ],
    exports: [
    ]
})
export class LandingModule {
}