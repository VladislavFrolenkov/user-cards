import { Route } from "@angular/router";
import { LandingComponent } from './containers/landing/landing.component';

export const routes: Route[] = [
    { path: '', component: LandingComponent }
]