import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

export interface IDonationSchema {
  icon: string;
  name: string;
  explorerUrl: string;
  address: string;
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public lastName: string = "";
  
  constructor(
    private _matDialog: MatDialog
  ) { }

  ngOnInit() {
  }

}
