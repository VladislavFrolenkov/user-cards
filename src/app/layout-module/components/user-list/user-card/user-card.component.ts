import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

export enum SOCIAL_CODE {
  VK = 'vk',
  FB = 'fb'
}

export enum STATE_CODE {
  NEW = 'new',
  PROC = 'proc',
  CONF = 'conf',
  ERR = 'err'
}

export interface IAmUser {
  name: string;
  soname: string;
  email: string;
  avatar: string;
  status: boolean;
  role: string;
  description: string;
  isReadMore: boolean;
  state: string;
}

export interface IAmForms {
  firstName: string;
  secondSoname: string;
}

export interface IAmSocial {
  social: string;
  name: string;
}

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})

export class UserCardComponent implements OnInit {

  public firstName: string = "";
  public secondSoname: string = "";
  public forLableN: string = "Имя:";
  public forLableS: string = "Фамилия:";

  public user: IAmUser[] = [
    {
      name: 'Имя_111',
      soname: 'Фамилия_111',
      email: 'email111e@emailemail.com',
      avatar: 'assets/images/avatar0.jpg',
      status: true,
      role: 'teacher',
      description: 'test description',
      isReadMore: true,
      state: 'conf',
    },
    {
      name: 'Имя_222',
      soname: 'Фамилия_222',
      email: 'email222@email222.com',
      avatar: 'assets/images/avatar0.jpg',
      status: true,
      role: 'admin',
      description: 'Ваше имя и фамилия будут видеть в чатах все пользователи и преподаватели платформы Wixeda.',
      isReadMore: false,
      state: 'err',
    },
    {
      name: 'Имя_333',
      soname: 'Фамилия_333',
      email: 'email333@email333.com',
      avatar: 'assets/images/avatar0.jpg',
      status: false,
      role: 'student',
      description: 'Ваше имя и фамилия не существуют.',
      isReadMore: false,
      state: 'proc',
    }
  ];

  public socials: IAmSocial[] = [
    {
      social: 'assets/svg/socialvk.svg',
      name: 'vk'
    },
    {
      social: '/assets/svg/socialfacebook.svg',
      name: 'fb'
    }
  ]

  public formField: IAmForms;

  constructor() { }

  ngOnInit(): void {
  }

  addLabelEntry() {
    this.formField = { firstName: this.firstName, secondSoname: this.secondSoname }
  }

  state(userState: string) {
    switch (userState) {
      case STATE_CODE.NEW: return 'новый';
      case STATE_CODE.PROC: return 'обработка';
      case STATE_CODE.CONF: return 'подтверждён';
      case STATE_CODE.ERR: return 'ошибка';
    }
  }
}
