import { Component, OnInit, Input } from '@angular/core';

export interface IAmUser {
  name: string;
  soname: string;
  email: string;
  avatar: string;
  status: boolean;
  role: string;
  description: string;
  isReadMore: boolean;
  state: string;
}

export interface IAmSocial {
  social: string;
  name: string;
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})

export class UserListComponent implements OnInit {

  @Input()
  firstName: string;

  @Input()
  secondSoname: string;

  @Input()
  forLableN: string;

  @Input()
  forLableS: string;

  @Input()
  user: IAmUser[];

  @Input()
  socials: IAmSocial[];

  constructor() { }

  ngOnInit() {
  }

}