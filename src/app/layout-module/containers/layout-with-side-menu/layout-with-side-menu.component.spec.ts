import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutWithSideMenuComponent } from './layout-with-side-menu.component';

describe('LayoutWithSideMenuComponent', () => {
  let component: LayoutWithSideMenuComponent;
  let fixture: ComponentFixture<LayoutWithSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutWithSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutWithSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
