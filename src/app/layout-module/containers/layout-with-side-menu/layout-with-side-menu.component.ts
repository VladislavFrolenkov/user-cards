import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { takeWhile, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-layout-with-side-menu',
  templateUrl: './layout-with-side-menu.component.html',
  styleUrls: ['./layout-with-side-menu.component.scss']
})
export class LayoutWithSideMenuComponent implements OnInit {

  public states = {
    isAlive: true
  }

  constructor(
    private _toastr: ToastrService
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.states.isAlive = false;
  }

}
