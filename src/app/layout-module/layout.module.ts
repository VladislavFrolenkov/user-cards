import { NgModule } from "@angular/core";
import { PublicLayoutComponent } from './containers/public-layout/public-layout.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { FooterComponent } from './components/footer/footer.component';
import { CommonModule } from '@angular/common';
import { LayoutWithSideMenuComponent } from './containers/layout-with-side-menu/layout-with-side-menu.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@ui/form-field/form-field.module';
import { ButtonModule } from '@ui/button';
import { UserFormComponent } from "./components/user-form/user-form.component";
import { UserListComponent } from './components/user-list/user-list.component';
import { UserCardComponent } from './components/user-list/user-card/user-card.component';

@NgModule({
    imports: [
        CommonModule,
        InlineSVGModule,
        RouterModule,
        MatDialogModule,
        FormsModule,
        FormFieldModule,
        ButtonModule,
    ],
    declarations: [
        PublicLayoutComponent,
        LayoutWithSideMenuComponent,
        HeaderComponent,
        FooterComponent,
        UserFormComponent,
        UserListComponent,
        UserCardComponent
    ],
    exports: [
        PublicLayoutComponent,
        LayoutWithSideMenuComponent,
        FooterComponent,
        HeaderComponent,
        UserFormComponent,
        UserListComponent,
        UserCardComponent
    ],
    entryComponents: [
    ]
})
export class LayoutModule {

}