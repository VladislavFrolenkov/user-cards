import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { WalletService } from './wallet.service';

@Injectable({
    providedIn: 'root'
})
export class AccessGuard implements CanActivate {
    constructor(
        private router: Router,
        private walletService: WalletService
    ) {
    }
    
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.walletService.wallet) {
            this.router.navigate(['/wallet/access']);
            return false;
        }
        return true;
    }
}