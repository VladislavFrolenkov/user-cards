import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


export interface IFeed {
    author: string;
    description: string;
    image: string;
    link: string;
    title: string;
    url: string;
}

export interface IFeedItem {
    author: string;
    categories: string[];
    content: string;
    description: string;
    enclosure: any;
    guid: string;
    link: string;
    pubDate: string;
    thumbnail: string;
    title: string;
}

export interface IRssFeedRespose {
    feed: IFeed;
    items: IFeedItem[];
}

@Injectable({
    providedIn: 'root'
})
export class IdenaFeedService {

    private _feed$$: BehaviorSubject<IFeedItem[]> = new BehaviorSubject(null);
    public feed$ = this._feed$$.asObservable();

    constructor(
        private _http: HttpClient
    ) {
        this.loadIdenaFeed();
        setInterval(this.loadIdenaFeed.bind(this), 1000 * 60 * 30);
    }

    async loadIdenaFeed() {
        this._http.get<IRssFeedRespose>(`https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/@idena.network`).pipe(
            map(r => r.items.map(item => ({
                    ...item,
                    description: item.description.replace(/<[^>]*>?/gm, '')
                }))
            )            
        )
        .subscribe((v) => this._feed$$.next(v));
    }
}