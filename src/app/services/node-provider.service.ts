import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cloneDeep as _cloneDeep} from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

export interface IIdenaResponse<T> {
    error: {
        code: number;
        message: string;
    }
    jsonrpc: string;
    id: number;
    result: T;
}

export interface INodeForm {
    address: string,
    port: number,
    apiKey: string
}

export interface INodeInLS {
    address: string,
    port : number,
    active: boolean,
    key: string
}

@Injectable({
    providedIn: 'root'
})
export class NodeProvider {
    private _id: number = -1;
    public storageNodes: INodeInLS[] = [{
        address: 'https://node.idenawallet.io',
        port : 9009,
        active: false,
        key: '7d803ca89a09341b80e5e3349a5d178f'
    }];
    public currentNode: INodeInLS;
    private _isConnected$$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public isConnected$: Observable<boolean> = this._isConnected$$.asObservable().pipe(distinctUntilChanged());

    constructor(
        private readonly _http: HttpClient,
    ) {
        if(window.localStorage.getItem('storageNodesList')) {
            this.storageNodes = _cloneDeep(JSON.parse(window.localStorage.getItem('storageNodesList')));
            this.currentNode = _cloneDeep(this.storageNodes.find(item => item.active))
        } else {
            this.activateNode(0);
        }

    }

    async makeRequest<T>(method: string, params: Array<any>, node: INodeInLS = this.currentNode): Promise<T> {
        this._id++;
        try {
            let response = await this._http.post<IIdenaResponse<T>>(`${node.address}:${node.port}`, {
                method: method, 
                params: params, 
                id: this._id, 
                key: node.key
            }).toPromise();
            node === this.currentNode && this._isConnected$$.next(true);
            if (response.error) {
                throw new Error(`[code: ${response.error.code}]: ${response.error.message}`);
            }
            return response.result;
        } catch(e) {
            node === this.currentNode && this._isConnected$$.next(false);
            throw e;
        }
    }

    removeNodeFromLS(index: number) {
        this.storageNodes.splice(index, 1);
        if(this.storageNodes.length === 1) {
            this.storageNodes[0].active = true;
            this.currentNode = _cloneDeep(this.storageNodes[0]);
        }
        this.updateLocalStorage();
    }

    updateLocalStorage() {
        window.localStorage.setItem('storageNodesList', JSON.stringify(
            this.storageNodes
        ));
    }

    activateNode(index) {
        this.storageNodes.map(item => {
            item.active = false;
        })
        this.storageNodes[index].active = true;
        this.currentNode = _cloneDeep(this.storageNodes[index]);
        this.updateLocalStorage();
    }

    addNodeToLS(form: INodeInLS) {
        this.storageNodes.push(form);
        this.updateLocalStorage();
    }

    async nodeHealthCheck(nodeForCheck: INodeInLS) {
        try {
            await this.makeRequest<null>('dna_epoch', [], nodeForCheck);
            return true;
        } catch(e) {
            return false;
        }
    }
}