import { Injectable } from "@angular/core";
import { WalletService } from './wallet.service';
import { HttpClient } from '@angular/common/http';

export interface ITransaction {
    amount: string;
    fee: string;
    from: string;
    hash: string;
    maxFee: string;
    size: number;
    timestamp: string;
    tips: string;
    to: string;
    type: string;
}


@Injectable({
    providedIn: 'root'
})
export class TransactionService {

    constructor(
        private readonly _wallet: WalletService,
        private readonly _http: HttpClient
    ) {

    }

    async loadTxs(page: number, pageSize: number) {
        // имитируем нормальную пагинированную выдачу
        let totalCount = await this._http.get<{result: number}>(`https://api.idena.org/api/Address/${this._wallet.wallet.address}/Txs/Count`).toPromise()
        let response =  await this._http.get<{result: ITransaction[]}>(`https://api.idena.org/api/Address/${this._wallet.wallet.address}/Txs?skip=${(page - 1) * pageSize}&limit=${pageSize}`).toPromise();
        return {
            totalCount: totalCount.result,
            items: response.result ? response.result : [],
            page,
            pageSize
        }
    }
}