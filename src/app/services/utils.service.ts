import { Injectable } from "@angular/core";
import { ISendTransactionForm } from './wallet.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class Utils {

    constructor(
        private _toastr: ToastrService
    ) {

    }

    stringifySendTransactionForm(form: ISendTransactionForm): string {
        return `${form.address}?asset=DNA&value=${form.amount || 0}`;
    }

    parseSendTransactionForm(str: string): ISendTransactionForm {
        let validationRegExp = /0x[A-z0-9]*\?asset=DNA&value=[0-9]*/gm;
        let isValid = validationRegExp.test(str);
        if (!isValid) {
            throw new Error('invalid payment code');
        }
        let address = str.split('?')[0];
        let amount = parseFloat(str.split('value=')[1]);
        return {
            amount: amount,
            address: address
        }
    }

    shareQRCode(file: File) {
        let navigator: any = window.navigator;
        if (navigator.canShare && navigator.canShare([file])) {
            navigator.share({
                files: [file]
            });
        } else {
            this._toastr.error('Web share api unavailable');
        }
    }

    async share(qrcode: string, address: string) {
        let qrfileResponse = await fetch(qrcode);
        let blob = await qrfileResponse.blob();
        this.shareQRCode(new File([blob], `receive-request-${address}`,{ type: "image/gif" }));
    }
}