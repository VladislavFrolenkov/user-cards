import { Injectable } from "@angular/core";
import { Wallet, utils } from 'ethers';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, interval } from 'rxjs';
import { NodeProvider } from './node-provider.service';
import { Buffer } from 'buffer';
import { encode as rlpEncode } from 'rlp';
import { takeWhile } from 'rxjs/operators';

// ход конем, чтобы rlpEncode работал
window['Buffer'] = Buffer;

export interface IAccessKeystoreForm {
    keystore: string;
    pk: string;
}

export interface IBalance {
    stake: string;
    balance: string;
    nonce: number;
}

export interface IEpoch {
    epoch: number;
    nextValidation: string;
    currentPeriod: string;
    currentValidationStart: string;
}

export interface ISendTransactionForm {
    address: string;
    amount: number;
}


@Injectable({
    providedIn: 'root'
})
export class WalletService {

    public wallet: Wallet = null;
    public balance$$: BehaviorSubject<IBalance> = new BehaviorSubject<IBalance>(null);
    public epoch: IEpoch = null;

    constructor(
        private readonly _router: Router,
        private readonly _toastr: ToastrService,
        private readonly _nodeProvider: NodeProvider
    ) {
        this.loadEpoch();
    }

    async createRandom() {
        await this._applyWallet(Wallet.createRandom());
        return this.wallet;
    }

    getKeystoreFile(pwd: string) {
        return this.wallet.encrypt(pwd);
    }

    verifyMnemonicWords(words: string[]): boolean {
        return this.wallet.mnemonic === words.join(' ');
    }

    async accessByPk(pk: string) {
        try {
            await this._applyWallet(new Wallet(pk));
            this._navigateToCabinet();
        } catch(e) {
            this._toastr.error('Invalid private key');
        }
    }

    async accessByKeystore(form: IAccessKeystoreForm) {
        try {
            let wallet = await Wallet.fromEncryptedJson(form.keystore, form.pk);
            await this._applyWallet(wallet);
            this._navigateToCabinet();
        } catch(e) {
            this._toastr.error('Invalid password or keystore file');
        }
    }

    async accessByMnemonic(words: string[]) {
        try {
            let wallet = Wallet.fromMnemonic(words.join(' '));
            await this._applyWallet(wallet);
            this._navigateToCabinet();
        } catch(e) {
            this._toastr.error('Invalid mnemonic phrase');
        }
    }

    async accessByEncodedPk(encrypted: string, keystr: string) {
        try {
            let keyarr = Buffer.from(keystr, 'hex');
          
            let key = await window.crypto.subtle.importKey(
              "raw",
              keyarr,
              "AES-GCM",
              false,
              ["encrypt", "decrypt"]
            );
          
            let data = Buffer.from(encrypted, 'hex');
          
            let iv = data.slice(0,12);
            let encr = data.slice(12);
          
            let decrypted = await crypto.subtle.decrypt({"name":"AES-GCM","iv":iv}, key, encr);
            let pk = Buffer.from(decrypted).toString('hex');
    
            await this._applyWallet(new Wallet(pk));
            this._navigateToCabinet();
        } catch(e) {
            this._toastr.error('Invalid words sequence.');
        }
    }

    async makeSendTransaction(form: ISendTransactionForm) {
        await this.loadEpoch();
        const data = [
            this.balance$$.value.nonce + 1, // nonce
            this.epoch.epoch, // epoch
            0, // type
            form.address, // to
            form.amount * Math.pow(10, 18), // amount (0.000000000000000001) 
            10000, // max fee (0.000000000000010000)
            null, // tips
            '0x', // payload (can be null too)
        ];
        
        const rlpData = rlpEncode(data);
        const hash = utils.keccak256(rlpData);
        let key = new utils.SigningKey(this.wallet.privateKey);
        
        const sig = key.signDigest(hash);
        
        const joinedSignature = Buffer.concat([
            Buffer.from(sig.r.substr(2), 'hex'),
            Buffer.from(sig.s.substr(2), 'hex'),
            Buffer.from([sig.recoveryParam]),
        ]);
        
        const res = [...data, joinedSignature];
        const rlpResult = rlpEncode(res);
        let response = await this._nodeProvider.makeRequest<string>('bcn_sendRawTx', [`0x${rlpResult.toString('hex')}`]);
        return response;
    }

    async loadBalance() {
        let balance = await this._nodeProvider.makeRequest<IBalance>('dna_getBalance', [this.wallet.address]);
        this.balance$$.next(balance);
    }

    async loadEpoch() {
        this.epoch = await this._nodeProvider.makeRequest<IEpoch>('dna_epoch', []);
    }

    private _navigateToCabinet() {
        this._router.navigate(['/app/home']);
    }

    private async _applyWallet(wallet: Wallet) {
        this.wallet = wallet;
        // обновляем каждые 10 сек баланс
        interval(1000 * 10).pipe(
            takeWhile(() => this.wallet !== null)
        ).subscribe(this.loadBalance.bind(this)) 
    }

    logout() {
        this.wallet = null;
        this.balance$$ = new BehaviorSubject<IBalance>(null);
        this.epoch = null;
        this._router.navigate(['/wallet/access']);
    }
}