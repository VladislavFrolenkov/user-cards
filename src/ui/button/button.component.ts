import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'idena-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input()
  public btnType: 'button' | 'submit' = 'button';

  @Input()
  public isDisabled: boolean = false;

  @Input()
  public isLoading: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
