import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { trigger, style, query, transition, animate, stagger } from '@angular/animations';


// сделать стаггер в обратную сторону с отрицательным таймингом как поправят https://github.com/angular/angular/issues/19786
@Component({
  selector: 'idena-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  animations: [
    trigger('itemsContainer', [
      transition('* => *', [
        query(':leave', [
          stagger(200, animate('400ms ease-in', style({
              transform: 'translateX({{direction}})',
              opacity: 0
            }))
          )
        ], {optional: true}),
        query(':enter', [
          style({opacity: 0}),
            stagger(200, animate('400ms ease-in', style({
              opacity: 1
            }))
          )
        ], {
          optional: true
        })
      ], {
        params: {
          direction: 'next',
          stagger: 200
        }
      })
    ])
  ]
})
export class CarouselComponent implements OnInit {
  @Input()
  public pageSize = 1; // кол-во элементов на странице

  @Input()
  public itemTpl: TemplateRef<any>;

  @Input()
  public pointsBlock: boolean = true;

  @Input()
  public set items(value: any[]) {
    this._items = value;
    this.lastPage = Math.ceil(this.items.length / this.pageSize) - 1;
    this.pagePoints = Array(this.lastPage + 1);
  }
  public get items() {
    return this._items;
  }

  private _items: any[];

  public pagePoints = [];

  public visiblityFlags = [];

  public currentPage = 0;

  public lastPage = 0;

  public trigger = {
    value: '',
    params: {
      direction: '-200px'
    }
  };

  public isAnimating = false;
  

  constructor(
  ) { }

  public ngOnInit() {
    this.visiblityFlags = this.items.map((value, index) => index >= this.pageSize ? false : true);
  }


  public next() {
    if (this.lastPage === this.currentPage || this.isAnimating) {
      return;
    }
    this.currentPage++;
    this.trigger = {
      value: 'leave',
      params: {
        direction: '-200px'
      }
    };
    this.visiblityFlags = this.visiblityFlags.map(() => false);
    this.isAnimating = true;
  }
  
  public update(data) {
    if (data.toState === 'leave') {
      this.trigger = {
        value: 'enter',
        params: {
          direction: this.trigger.params.direction
        }
      }
      this.visiblityFlags = this.visiblityFlags.map((value, index) => {
        if (
          this.currentPage * this.pageSize <= index &&
          (this.currentPage + 1) * this.pageSize > index
        ) {
          return true;
        } else {
          return false;
        }
      });
      this.isAnimating = false;
    }
  }

  public prev() {
    if (this.currentPage === 0 || this.isAnimating) {
      return;
    }
    this.isAnimating = true;
    this.currentPage--;
    this.trigger = {
      value: 'leave',
      params: {
        direction: '200px'
      }
    };
    this.visiblityFlags = this.visiblityFlags.map(() => false);
  }

  public goToPage(page: number) {
    if (page == this.currentPage) {
      return;
    }

    this.isAnimating = true;
    this.trigger = {
      value: 'leave',
      params: {
        direction: page > this.currentPage ? '-200px' : '200px'
      }
    };
    this.visiblityFlags = this.visiblityFlags.map(() => false);
    this.currentPage = page;
  }

}
