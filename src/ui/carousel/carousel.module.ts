import { NgModule } from "@angular/core";
import { CarouselComponent } from "./carousel.component";
import { CommonModule } from "@angular/common";
import { InlineSVGModule } from "ng-inline-svg";

@NgModule({
    imports: [
        CommonModule,
        InlineSVGModule
    ],
    declarations: [ CarouselComponent ],
    exports: [ CarouselComponent ]
})
export class CarouselModule {

}