import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FileDropAreaComponent } from './file-drop-area/file-drop-area.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { ButtonModule } from '@ui/button';
import { FormFieldModule } from '@ui/form-field/form-field.module';

@NgModule({
    imports: [
        CommonModule,
        InlineSVGModule,
        ButtonModule,
        FormFieldModule
    ],
    declarations: [FileDropAreaComponent],
    exports: [FileDropAreaComponent]
})
export class FileDropAreaModule {

}