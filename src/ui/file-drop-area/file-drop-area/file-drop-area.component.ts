import { Component, OnInit, ViewChild, ElementRef, forwardRef, Injector, Optional } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, NgForm } from '@angular/forms';

@Component({
  selector: 'idena-file-drop-area',
  templateUrl: './file-drop-area.component.html',
  styleUrls: ['./file-drop-area.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => FileDropAreaComponent), multi: true }
  ]
})
export class FileDropAreaComponent implements OnInit, ControlValueAccessor {

  @ViewChild('fileinput', { read: ElementRef, static: true })
  private _fileinput: ElementRef<HTMLInputElement>;

  private _lastEntered: HTMLElement;
  private _onChange: (val: string) => void;
  private _onTouch: () => void;
  public ngControl: NgControl;
  public states = {
    isOver: false,
    isFileLoaded: false,
    isInvalidFile: false,
  }

  constructor(
    private readonly _injector: Injector,
    @Optional() public readonly ngForm: NgForm,
  ) { }

  ngOnInit() {
    this.ngControl = this._injector.get(NgControl);
  }

  writeValue() {
    // пока нет необходимости, поэтому пустой

  }

  registerOnChange(fn) {
    this._onChange = fn;
  }

  registerOnTouched(fn) {
    this._onTouch = fn;
  }

  onFile() {
    this._handleFile(this._fileinput.nativeElement.files[0]);
  }

  onDragEnter($event) {
    $event.stopPropagation();
    $event.preventDefault();
    this._lastEntered = $event.target;
    this.states.isOver = true;
  }

  onDragLeave($event) {
    $event.stopPropagation();
    $event.preventDefault();
    if ($event.target === this._lastEntered) {
      this.states.isOver = false;
    }
  }

  onDrop(e: DragEvent) {
    e.stopPropagation();
    e.preventDefault();
    this._handleFile(e.dataTransfer.files[0]);
    this.states.isOver = false;
  }

  onDragOver(e: DragEvent) {
    e.stopPropagation();
    e.preventDefault();
  }

  reset() {
    this.states.isFileLoaded = false;
    this._onChange(null);
  }

  private _handleFile(file: File) {
    // т.к. пока этот контрол юзается только для аплоада keystore файла, 
    // поэтому валидацию зашил прямо сюда
    let reader = new FileReader();
    reader.onload = (e: Event) => {
      try {
        JSON.parse(reader.result as string);
        this._onChange(reader.result as string);
        this.states.isInvalidFile = false;
        this.states.isFileLoaded = true;
      } catch (e) {
        this.states.isInvalidFile = true;
        this.states.isFileLoaded = false;
        return;
      }
    };
    reader.readAsText(file);
  }

}
