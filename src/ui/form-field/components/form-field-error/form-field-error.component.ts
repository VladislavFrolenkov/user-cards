import { Component, OnInit, Optional, Input, SimpleChanges } from '@angular/core';
import { NgForm, NgControl } from '@angular/forms';
import { FormFieldComponent } from './../form-field/form-field.component';
import { filter, takeWhile, delay } from 'rxjs/operators';
import { merge } from 'rxjs';

export interface IValidationMessages {
    [validationName: string]: string;
}

@Component({
    selector: 'form-field-error',
    templateUrl: './form-field-error.component.html',
    styleUrls: ['./form-field-error.component.scss']
})
export class FormFieldErrorComponent implements OnInit {
    public errorMsg = '';

    @Input()
    public set control(control: NgControl) {
        this._control = control;
        if (control && this.ngForm) {
            this.init();
        }
    }
    public get control() {
        return this._control;
    }

    @Input()
    public customValidationMessages = {};

    public validationMessages: IValidationMessages = {};
    private _defaultValidationMessages = {
        'required': 'This field is required',
        'validateAddress': 'Inalid address',
        'validatePassword': 'Needed 8 characters',
        'positiveNumber': 'Amount should be greater then zero',
        'paymentCode': 'Invalid payment code',
        'validateEqual': 'Invalid value',
        'validateKeystore': 'Invalid keystore file'
    }
    private _control: NgControl;
    public isAlive = true;

    constructor(
        @Optional() private ngForm: NgForm,
        @Optional() private _ff: FormFieldComponent
    ) { }

    ngOnInit() {
        this.control = this._ff ? this._ff.control.control : this.control;
        this._mergeValidationMessages();
    }

    ngOnDestroy() {
        this.isAlive = false;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.customValidationMessages) {
            this._mergeValidationMessages();
        }
    }

    init() {
        merge(this.control.statusChanges, this.ngForm.ngSubmit)
        .pipe(
            filter((v) => this.ngForm.submitted && !!this.control),
            delay(0), // delay специально, т.к. statusChanges эмитит раньше чем флаг submitted  сбрасывается в  false после resetForm
            takeWhile(() => this.isAlive)
        )
        .subscribe(() => {
            if (!this.control) {
                return;
            }
            this.errorMsg = this.updateMessage();
        })
    }
    

    public updateMessage() {
        if (!this.control || !this.control.errors || !this.ngForm.submitted) {
            return '';
        }
        let errorKeys = Object.keys(this.control.errors);
        if (errorKeys.length) {
          return this.validationMessages[errorKeys[0]];
        } else {
          return '';
        }
    }

    private _mergeValidationMessages() {
        this.validationMessages = Object.assign({}, this._defaultValidationMessages, this.customValidationMessages || {});
    }

}
