import { Component, Input, ContentChild, OnInit, Optional } from '@angular/core';
import { FormControl } from '../../directives/form-control';
import { IValidationMessages } from '../form-field-error/form-field-error.component';
import { NgForm } from '@angular/forms';
import { filter, delay, takeWhile, map, distinctUntilChanged } from 'rxjs/operators';
import { merge } from 'rxjs';

@Component({
    selector: 'idena-form-field',
    templateUrl: 'form-field.component.html',
    styleUrls: ['form-field.component.scss']
})
export class FormFieldComponent implements OnInit {
    
    @ContentChild(FormControl) 
    public control: FormControl;

    @Input()
    public label: string;

    @Input()
    public customValidationMessages: IValidationMessages;

    @Input()
    public noValidationMessages: boolean = false;

    public states = {
        isAlive: true,
        isInited: true,
        isSubmitted: false
    }
    
    constructor(
        @Optional() private  ngform: NgForm
    ) {}

    ngOnInit() {
        if (!this.ngform) {
            return;
        }

        merge(this.ngform.statusChanges, this.ngform.ngSubmit)
        .pipe(
            delay(0), // delay специально, т.к. statusChanges эмитит раньше чем флаг submitted  сбрасывается в  false после resetForm
            map(() => this.ngform.submitted),
            distinctUntilChanged(),
            takeWhile(() => this.states.isAlive)
        )
        .subscribe(() => {
            this.states.isSubmitted = this.ngform.submitted;
        })

        // хак, нужный для того, чтобы заставить хром стартануть отрисовку анимации, 
        // без него самый первый transition почему-то замирает на неопределенно время и label перекрывает (не уходит на верх) содержимое контрола
        // в других браузерах норм отрабатывает, а в хроме вот такой косяк
        setTimeout(() => this.states.isInited = true, 0)
    }

}
