import { NgControl } from "@angular/forms";

export abstract class FormControl {
    errorState: boolean;
    valid: boolean;
    empty: boolean;
    focused: boolean;
    disabled: boolean;
    control?: NgControl;

    constructor() {
    }
}
