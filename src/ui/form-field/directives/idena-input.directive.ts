import { Directive, HostListener, Input, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import { FormControl } from './form-control';

@Directive({
    selector: `input[idenaInput], textarea[idenaInput]`,
    exportAs: 'idenaInput',
    providers: [{provide: FormControl, useExisting: IdenaInputDirective}],
})
export class IdenaInputDirective extends FormControl {
    public isFocused: boolean = false;

    constructor(
        private _control: NgControl,
        public elementRef: ElementRef,
    ) {
        super();
    }

    ngOnInit() {}

    @HostListener('blur')
    public onBlur() {
        this.isFocused = false;
    }

    @HostListener('focus')
    public onFocus() {
        this.isFocused = true;
    }

    @Input()
    public placeholder: string;

    public get control(): NgControl { return this._control; }
    get errorState() {return this._control.invalid; }
    get valid() {return this._control.valid; }
    get empty() { return (this.control.value === null || this.control.value === undefined || this.control.value === '') && !this.placeholder; }
    get focused() { return this.isFocused && !this.getDisabledState(); }
    get disabled() { return this.getDisabledState(); }

    private getDisabledState(): boolean {
        let value = this.elementRef.nativeElement.hasAttribute('disabled');
        return value;
    }
}
