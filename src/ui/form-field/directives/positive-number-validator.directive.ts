import { Directive, forwardRef, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[positiveNumber][formControlName],[positiveNumber][formControl],[positiveNumber][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => PositiveNumberValidator), multi: true }
    ]
})
export class PositiveNumberValidator implements Validator {

    @Input()
    public allowZero = true;

    constructor(
    ) { 
    }

    validate(control: AbstractControl): { [key: string]: any } {
        if (control.value === '' || control.value === null || control.value === undefined) {
            return null;
        }

        if (isNaN(control.value) || control.value < 0 || (!this.allowZero && control.value === 0)) {
            return {
                positiveNumber: false
            }
        } 

        return null;
    }
}