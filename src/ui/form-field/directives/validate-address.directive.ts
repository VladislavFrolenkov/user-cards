import { Directive, Attribute, forwardRef } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { utils } from 'ethers';

@Directive({
    selector: '[validateAddress][formControlName],[validateAddress][formControl],[validateAddress][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidateAddressDirective), multi: true }
    ]
})
export class ValidateAddressDirective implements Validator {
    public isFocused: boolean = false;

    constructor(
    ) { 
    }

    validate(c: AbstractControl): { [key: string]: any } {
        // self value
        var v: any;
        try {
            v = utils.getAddress(c.value)
          } catch(e) { 
            v = null
          }
        
        
        // // value not equal
        if (!v) {
            return {validateAddress: false};
        }

         return null;
    }
}
