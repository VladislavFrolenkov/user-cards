import { Directive, forwardRef, Input } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
    selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidateEqualDirective), multi: true }
    ]
})
export class ValidateEqualDirective implements Validator {
    public isFocused: boolean = false;

    @Input('validateEqual')
    public value: string;

    constructor(
    ) { 
    }

    validate(c: AbstractControl): { [key: string]: any } {
        if (this.value && c.value !== this.value) {
            return {validateEqual: false};
        }

         return null;
    }
}
