import { Directive, forwardRef, Input } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
    selector: '[validateKeystore][formControlName],[validateKeystore][formControl],[validateKeystore][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidateKeystoreDirective), multi: true }
    ]
})
export class ValidateKeystoreDirective implements Validator {

    constructor(
    ) { 
    }

    validate(c: AbstractControl): { [key: string]: any } {
        if (!c.value) {
            return null;
        }

        try {
            JSON.parse(c.value);
        } catch(e) {
            return {validateKeystore: false}
        }

        return null;
    }
}
