import { Directive, HostListener, Input, ElementRef, Attribute, forwardRef } from '@angular/core';
import { NgControl, Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { Wallet, utils } from 'ethers';

@Directive({
    selector: '[validatePassword][formControlName],[validatePassword][formControl],[validatePassword][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidatePasswordDirective), multi: true }
    ]
})
export class ValidatePasswordDirective implements Validator {
    public isFocused: boolean = false;

    constructor(
        @Attribute('validatePassword') private validatePassword: string,
    ) { }

    validate(c: AbstractControl): { [key: string]: any } {

        // // value not equal
        if (!c.value) {
            return {validatePassword: false};
        }
        if (c.value.length < 8) {
            return {validatePassword: false};
        }

         return null;
    }
}
