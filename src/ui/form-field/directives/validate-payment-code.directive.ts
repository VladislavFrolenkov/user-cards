import { Directive, forwardRef } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { Utils } from 'app/services/utils.service';

@Directive({
    selector: '[validatePaymentCode][formControlName],[validatePaymentCode][formControl],[validatePaymentCode][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidatePaymentCodeDirective), multi: true }
    ]
})
export class ValidatePaymentCodeDirective implements Validator {
    public isFocused: boolean = false;

    constructor(
        private _utils: Utils
    ) { 
        console.log(this);
    }

    validate(c: AbstractControl): { [key: string]: any } {  
        try {
            this._utils.parseSendTransactionForm(c.value);
            return null;
        } catch(e) {
            return {paymentCode: false}
        }
    }
}
