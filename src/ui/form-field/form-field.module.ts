import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FormFieldErrorComponent } from './components/form-field-error/form-field-error.component';
import { FormFieldComponent } from './components/form-field/form-field.component';
import { IdenaInputDirective } from './directives/idena-input.directive';
import { ValidateAddressDirective } from './directives/validate-address.directive';
import { ValidatePasswordDirective } from './directives/validate-password.directive';
import { PositiveNumberValidator } from './directives/positive-number-validator.directive';
import { ValidatePaymentCodeDirective } from './directives/validate-payment-code.directive';
import { ValidateEqualDirective } from './directives/validate-equal.directive';
import { ValidateKeystoreDirective } from './directives/validate-keystore.directive';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        FormFieldComponent,
        IdenaInputDirective,
        FormFieldErrorComponent,
        ValidateAddressDirective,
        ValidatePasswordDirective,
        PositiveNumberValidator,
        ValidatePaymentCodeDirective,
        ValidateEqualDirective,
        ValidateKeystoreDirective
    ],
    exports: [
        FormFieldComponent,
        IdenaInputDirective,
        FormFieldErrorComponent,
        ValidateAddressDirective,
        ValidatePasswordDirective,
        PositiveNumberValidator,
        ValidatePaymentCodeDirective,
        ValidateEqualDirective,
        ValidateKeystoreDirective
    ],
    entryComponents: []
})
export class FormFieldModule {

}
