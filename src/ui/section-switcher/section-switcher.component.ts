import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';


export interface ISectionSwitchOption {
    label: string;
    value: string | number;
}

@Component({
    selector: 'vip-section-switcher',
    templateUrl: './section-switcher.component.html',
    styleUrls: ['./section-switcher.component.scss'],
    providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SectionSwitcherComponent), multi: true}]
})
export class SectionSwitcherComponent implements OnInit, ControlValueAccessor {
    @Input()
    public options: ISectionSwitchOption[];

    public onChange: (code: string | number) => void;
    public activeSectionCode: string | number;

    constructor() { }

    ngOnInit() {
    }

    writeValue(value) {
        this.activeSectionCode = value;
    }
    
    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched() {

    }

    selectSection(opt: ISectionSwitchOption) {
        this.activeSectionCode = opt.value;
        this.onChange(opt.value);
    }
}
