import { NgModule } from "@angular/core";
import { SectionSwitcherComponent } from "./section-switcher.component";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [SectionSwitcherComponent],
    exports: [SectionSwitcherComponent]
})
export class SectionSwitcherModule {

}