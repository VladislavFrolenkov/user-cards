import { Directive, ElementRef, Input, ComponentRef, HostListener } from "@angular/core";
import { OverlayConfig, Overlay, ConnectionPositionPair, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { TooltipComponent } from "./tooltip/tooltip.component";

export const DefaultConnectionPositionPair = new ConnectionPositionPair(
    {originX: 'center', originY: 'top'},
    {overlayX: 'center', overlayY: 'bottom'},
    0,
    -5
);

export const DefaultFallbackConnectionPositionPair = new ConnectionPositionPair(
    {originX: 'start', originY: 'bottom'},
    {overlayX: 'start', overlayY: 'top'},
    0,
    -5
)

@Directive({
    selector: '[idenaTooltip]'
})
export class TooltipDirective {

    @Input('idenaTooltipPosition')
    public overlayPosition: ConnectionPositionPair = DefaultConnectionPositionPair;

    @Input('idenaTooltipFallbackPosition') 
    public fallbackPosition: ConnectionPositionPair = DefaultFallbackConnectionPositionPair;

    @Input('idenaTooltip')
    public set content(value: string) {
        this._content = value;
        if (this.isVisible) {
            this.cmpRef.instance.content = value;
        }
    }
    public get content() {
        return this._content;
    }
    private _content: string;

    private cmpPortalRef: ComponentPortal<TooltipComponent>;
    public cmpRef: ComponentRef<TooltipComponent>;
    public overlayRef: OverlayRef;
    public isVisible = false;

    constructor(
        private _overlay: Overlay,
        public elRef: ElementRef,
    ) {

    }

    ngOnInit() {
        this.cmpPortalRef = new ComponentPortal<TooltipComponent>(TooltipComponent);
    }

    ngOnDestroy() {
        this.close();
    }

    @HostListener('mouseenter')
    public show() {
        if (this.isVisible) {
            return;
        }
        this.isVisible = true;
        if (!this.overlayRef) {
            this.overlayRef = this._overlay.create(this._buildConfig());
        }

        this.cmpRef = this.overlayRef.attach(this.cmpPortalRef);
        this.cmpRef.instance.content = this.content;
    }

    @HostListener('mouseleave')
    public close() {
        if (!this.isVisible) {
            return;
        }
        this.isVisible = false;
        this.overlayRef.detach();
        this.cmpRef = null;
    }


    private _buildConfig(): OverlayConfig {
        this.fallbackPosition = this.fallbackPosition || this.overlayPosition;
        let connectionPosition = this._overlay.position()
                                    .connectedTo(
                                        this.elRef,
                                        {originX: this.overlayPosition.originX, originY: this.overlayPosition.originY},
                                        {overlayX: this.overlayPosition.overlayX, overlayY: this.overlayPosition.overlayY}
                                    )
                                    .withFallbackPosition(
                                        {originX: this.fallbackPosition.originX, originY: this.fallbackPosition.originY},
                                        {overlayX: this.fallbackPosition.overlayX, overlayY: this.fallbackPosition.overlayY},
                                        this.fallbackPosition.offsetX,
                                        this.fallbackPosition.offsetY
                                    )
                                    .withOffsetX(this.overlayPosition.offsetX || 0)
                                    .withOffsetY(this.overlayPosition.offsetY || 0);
        
        const overlayConfig = new OverlayConfig({
            positionStrategy: connectionPosition,
            scrollStrategy: this._overlay.scrollStrategies.reposition(),
            hasBackdrop: false
        });

   
        return overlayConfig;
    }
}