import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OverlayModule } from "@angular/cdk/overlay";
import { TooltipDirective } from "./tooltip.directive";
import { TooltipComponent } from "./tooltip/tooltip.component";

@NgModule({
    imports: [
        CommonModule,
        OverlayModule,
    ],
    declarations: [
        TooltipComponent,
        TooltipDirective
    ],
    entryComponents: [
        TooltipComponent
    ],
    exports: [
        TooltipDirective
    ]
})
export class TooltipModule {

}