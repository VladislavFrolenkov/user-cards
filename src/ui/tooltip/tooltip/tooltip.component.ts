import { Component, Input } from "@angular/core";
import { transition, trigger, style, animate } from "@angular/animations";

@Component({
    selector: 'shui-tooltip',
    templateUrl: './tooltip.component.html',
    styleUrls: ['./tooltip.component.scss'],
    animations: [
        trigger('tooltip', [
            transition(':enter',[ 
                style({transform: 'scale(0)', opacity: 0}), animate('.15s ease-in', style({transform: 'scale(1)', opacity: 1}))
            ]),
            transition(':leave',[ 
                animate('.15s ease-in', style({transform: 'scale(0)', opacity: 0}))
            ]),
        ])
    ]
})
export class TooltipComponent {
    @Input()
    public content: string;

}